#ifndef DEF_MAINWINDOW
#define DEF_MAINWINDOW

#include <QtWidgets>
#include <QtWebKitWidgets>

class MainWindow: public QMainWindow {
    Q_OBJECT
    public:
        MainWindow();

    private:
        void createActions();
        void createMenus();
        void createToolBar();
        void createStateBar();
        void createMainWindow();

    private slots:
        void onLoad();

    private:
        QTabWidget *tabs_;
        QWebView *webPage_;

        QAction *quitAction_;
};

#endif
