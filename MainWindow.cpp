#include "MainWindow.h"

MainWindow::MainWindow(): QMainWindow() {
    showMaximized();
    createActions();
    createMenus();
    createToolBar();
    createStateBar();
    createMainWindow();
}

void MainWindow::createActions() {
    quitAction_ = new QAction("&Quit", this);
    quitAction_->setShortcut(QKeySequence("Ctrl+Q"));
    connect(quitAction_, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void MainWindow::createMenus() {
    QMenu *fileMenu = menuBar()->addMenu("&File");
    fileMenu->addAction(quitAction_);
}

void MainWindow::createToolBar() {
    QToolBar *toolbar = addToolBar("File");

}

void MainWindow::createStateBar() {

}

void MainWindow::createMainWindow() {
    QWidget *centralArea = new QWidget;
    QVBoxLayout *mainLayout = new QVBoxLayout;
    tabs_ = new QTabWidget;

    webPage_ = new QWebView;
    webPage_->load(QUrl("http://www.google.fr"));

    tabs_->addTab(webPage_, webPage_->title());
    mainLayout->addWidget(tabs_);
    centralArea->setLayout(mainLayout);

    setCentralWidget(centralArea);

    connect(webPage_, SIGNAL(loadFinished(bool)), this, SLOT(onLoad()));
}

void MainWindow::onLoad() {
    tabs_->setTabText(0, webPage_->title());
}
